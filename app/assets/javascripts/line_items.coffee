# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


updateTotal = ->
  selection_id = $('#line_item_pallet_id').val()
  $.getJSON '/pallets/' + selection_id + '/price', {}, (json, response) ->
    price1 = json['price']
    quantity1 = $('#line_item_quantity').val()
    total = price1 * quantity1
    total2 = total.toFixed(2)
    $('#line_item_line_item_total').val(total2)

$(document).on 'page:change', ->
  if $('#line_item_quantity').length > 0
    updateTotal()
  $('#line_item_quantity').change -> updateTotal()
  if $('#line_item_pallet_id').length > 0
    updateTotal()
  $('#line_item_pallet_id').change -> updateTotal()
  $('#LT_table').dataTable
    aaSorting: []
    responsive: true
    bDestroy: true
    pagingType: "full_numbers"
