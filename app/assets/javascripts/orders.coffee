# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ShowHide =->
  $('#hideshow').click ->
    $('#showhide').toggle 1000

$(document).on 'page:change', ->
  ShowHide()
  $('#order_table').dataTable
    aaSorting: []
    responsive: true
    bDestroy: true
    pagingType: "full_numbers"

  $('#buttontoshow').click ->
    $('#showhide').hide 1000
    return