# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ShowHide =->
  $('#hideshow').click ->
    $('#hideshow_sup').toggle 1000

$(document).on 'page:change', ->
  ShowHide()
  $('#sp').dataTable
    aaSorting: []
    responsive: true
    bDestroy: true
  $('#btn_sup').click ->
    $('#hideshow_sup').hide 1000
    return
