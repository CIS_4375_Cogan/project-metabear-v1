class PalletGradesController < ApplicationController
  before_action :set_pallet_grade, only: [:show, :edit, :update, :destroy]

  # GET /pallet_grades
  # GET /pallet_grades.json
  def index
    @pallet_grades = PalletGrade.all
  end

  # GET /pallet_grades/1
  # GET /pallet_grades/1.json
  def show
    @pallet_grades = PalletGrade.all
    respond_to do |format|
      format.html
      format.pdf do
        if @pallet_grade
          pdf = PalletGradesPdf.new(@pallet_status, @pallet, @pallet_grade, @runner, @dimension)
          send_data pdf.render, filename: "Pallet Grade Report.pdf", type: "application/pdf", disposition: "inline"
        end
      end
    end
  end

  # GET /pallet_grades/new
  def new
    @pallet_grade = PalletGrade.new
  end

  # GET /pallet_grades/1/edit
  def edit
  end

  # POST /pallet_grades
  # POST /pallet_grades.json
  def create
    @pallet_grade = PalletGrade.new(pallet_grade_params)

    respond_to do |format|
      if @pallet_grade.save
        format.html { redirect_to @pallet_grade, notice: 'Pallet grade was successfully created.' }
        format.json { render :show, status: :created, location: @pallet_grade }
      else
        format.html { render :new }
        format.json { render json: @pallet_grade.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pallet_grades/1
  # PATCH/PUT /pallet_grades/1.json
  def update
    respond_to do |format|
      if @pallet_grade.update(pallet_grade_params)
        format.html { redirect_to @pallet_grade, notice: 'Pallet grade was successfully updated.' }
        format.json { render :show, status: :ok, location: @pallet_grade }
      else
        format.html { render :edit }
        format.json { render json: @pallet_grade.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pallet_grades/1
  # DELETE /pallet_grades/1.json
  def destroy
    if @pallet_grade.pallets.count <= 0
    @pallet_grade.destroy
    respond_to do |format|
      format.html { redirect_to pallet_grades_url, notice: 'Pallet grade was successfully destroyed.' }
      format.json { head :no_content }
    end
  else
    redirect_to pallet_grades_path, notice: "Cannot delete pallet grade because apply to existing pallets"
  end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pallet_grade
      @pallet_grade = PalletGrade.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pallet_grade_params
      params.require(:pallet_grade).permit(:level, :description)
    end
end
