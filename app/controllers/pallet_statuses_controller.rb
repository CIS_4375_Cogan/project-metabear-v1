class PalletStatusesController < ApplicationController
  before_action :set_pallet_status, only: [:show, :edit, :update, :destroy]

  # GET /pallet_statuses
  # GET /pallet_statuses.json
  def index
    @pallet_statuses = PalletStatus.all
  end

  # GET /pallet_statuses/1
  # GET /pallet_statuses/1.json
  def show
    @pallet_statuses = PalletStatus.all
    respond_to do |format|
      format.html
      format.pdf do
        if @pallet_status
          pdf = PalletStatusesPdf.new(@pallet_status, @pallet, @material, @runner, @dimension)
          send_data pdf.render, filename: "Pallet Status Report.pdf", type: "application/pdf", disposition: "inline"
        end
      end
    end
  end

  # GET /pallet_statuses/new
  def new
    @pallet_status = PalletStatus.new
  end

  # GET /pallet_statuses/1/edit
  def edit
  end

  # POST /pallet_statuses
  # POST /pallet_statuses.json
  def create
    @pallet_status = PalletStatus.new(pallet_status_params)

    respond_to do |format|
      if @pallet_status.save
        format.html { redirect_to @pallet_status, notice: 'Pallet status was successfully created.' }
        format.json { render :show, status: :created, location: @pallet_status }
      else
        format.html { render :new }
        format.json { render json: @pallet_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pallet_statuses/1
  # PATCH/PUT /pallet_statuses/1.json
  def update
    respond_to do |format|
      if @pallet_status.update(pallet_status_params)
        format.html { redirect_to @pallet_status, notice: 'Pallet status was successfully updated.' }
        format.json { render :show, status: :ok, location: @pallet_status }
      else
        format.html { render :edit }
        format.json { render json: @pallet_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pallet_statuses/1
  # DELETE /pallet_statuses/1.json
  def destroy
    if @pallet_status.pallets.count <= 0
    @pallet_status.destroy
    respond_to do |format|
      format.html { redirect_to pallet_statuses_url, notice: 'Pallet status was successfully destroyed.' }
      format.json { head :no_content }
    end
    else
      redirect_to pallet_statuses_path, notice: "Cannot delete pallet status because apply to existing pallets"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pallet_status
      @pallet_status = PalletStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pallet_status_params
      params.require(:pallet_status).permit(:description)
    end
end
