class PalletsController < ApplicationController
  before_action :set_pallet, only: [:show, :edit, :update, :destroy]

  # GET /pallets
  # GET /pallets.json
  def index
    @pallets = Pallet.all
    @suppliers = Supplier.all
  end

  def price
    logger.info "PRICE CALLED"
    pallet = Pallet.find(params[:id])
    render json:pallet
  end

  # GET /pallets/1
  # GET /pallets/1.json
  def show
  end

  # GET /pallets/new
  def new
    @pallet = Pallet.new
  end

  # GET /pallets/1/edit
  def edit
  end

  # POST /pallets
  # POST /pallets.json
  def create
    @pallet = Pallet.new(pallet_params)

    respond_to do |format|
      if @pallet.save
        format.html { redirect_to @pallet, notice: 'Pallet was successfully created.' }
        format.json { render :show, status: :created, location: @pallet }
      else
        format.html { render :new }
        format.json { render json: @pallet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pallets/1
  # PATCH/PUT /pallets/1.json
  def update
    respond_to do |format|
      if @pallet.update(pallet_params)
        format.html { redirect_to @pallet, notice: 'Pallet was successfully updated.' }
        format.json { render :show, status: :ok, location: @pallet }
      else
        format.html { render :edit }
        format.json { render json: @pallet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pallets/1
  # DELETE /pallets/1.json
  def destroy
    @pallet.update(:pallet_status_id => '2')
    respond_to do |format|
      format.html { redirect_to pallets_url, notice: 'Pallet was successfully .' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pallet
      @pallet = Pallet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pallet_params
      params.require(:pallet).permit(:pallet_grade_id, :pallet_status_id, :comment, :price, :material_id, :dimension_id, :runner_id, supplier_ids: [])
    end
end
