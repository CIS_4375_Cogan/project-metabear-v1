class ReportsController < ApplicationController


  def new_orders_monthly
    @orders = Order.all
    respond_to do |format|
      format.html
      format.pdf do

          pdf = NewOrdersMonthlyPdf.new(@order_status, @order)
          send_data pdf.render, filename: "New Orders Monthly.pdf", type: "application/pdf", disposition: "inline"

      end
    end
  end

  def cancelled_orders_monthly
    @order = Order.all
    respond_to do |format|
      format.html
      format.pdf do
        pdf = CancelledOrderMonthlyPdf.new(@order_status, @order)
        send_data pdf.render, filename: "Cancelled Orders Monthly.pdf", type: "application/pdf", disposition: "inline"
      end
    end
  end

  def inactive_customers_monthly
    @customer= Customer.all
    respond_to do |format|
      format.html
      format.pdf do
        pdf = InactiveCustomersMonthlyPdf.new(@customer, @customer_status)
        send_data pdf.render, filename: "Inactive Customers Monthly.pdf", type: "application/pdf", disposition: "inline"
      end
    end
  end

  def inactive_suppliers_monthly
    @supplier = Supplier.all
    respond_to do |format|
      format.html
      format.pdf do
        pdf = InactiveSuppliersMonthlyPdf.new(@supplier, @supplier_status)
        send_data pdf.render, filename: "Inactive Suppliers Monthly.pdf", type: "application/pdf", disposition: "inline"
      end
    end
  end

  def new_customers_monthly
    @customer = Customer.all
    respond_to do |format|
      format.html
      format.pdf do
        pdf = NewCustomersMonthlyPdf.new(@customer, @customer_status)
        send_data pdf.render, filename: "New Customers Monthly.pdf", type: "application/pdf", disposition: "inline"
      end
    end
  end

  def new_suppliers_monthly
    @supplier = Supplier.all
    respond_to do |format|
      format.html
      format.pdf do
        pdf = NewSuppliersMonthlyPdf.new(@supplier, @supplier_status)
        send_data pdf.render, filename: "New Suppliers Monthly.pdf", type: "application/pdf", disposition: "inline"
      end
    end
  end

  # def inactive_suppliers
  #   @supplier = Supplier.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @supplier
  #         pdf = InactiveSupplierPdf.new(@supplier, @supplier_status)
  #         send_data pdf.render, filename: "Inactive Suppliers Report.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def active_customers
  #   @customer = Customer.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @customer
  #         pdf = ActiveCustomerPdf.new(@customer, @customer_status)
  #         send_data pdf.render, filename: "Active Customers Report.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def inactive_customers
  #   @customer = Customer.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #     if @customer
  #       pdf = InactiveCustomerPdf.new(@customer, @customer_status)
  #       send_data pdf.render, filename: "Inactive Customers Report.pdf", type: "application/pdf", disposition: "inline"
  #     end
  #     end
  #   end
  # end
  #
  # def in_stock_pallets
  #   @pallet = Pallet.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @pallet
  #         pdf = InStockPalletsPdf.new(@pallet, @pallet_status)
  #         send_data pdf.render, filename: "Pallets In Stock.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def out_of_stock_pallets
  #   @pallet = Pallet.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @pallet
  #         pdf = OutOfStockPalletsPdf.new(@pallet, @pallet_status)
  #         send_data pdf.render, filename: "Pallets Out of Stock.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def backordered_pallets
  #   @pallet = Pallet.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @pallet
  #         pdf = BackorderedPalletsPdf.new(@pallet, @pallet_status)
  #         send_data pdf.render, filename: "Backordered Pallets.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def approved_orders
  #   @order = Order.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @order
  #         pdf = ApprovedOrderStatusPdf.new(@order, @order_status)
  #         send_data pdf.render, filename: "Approved Orders.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def backordered_orders
  #   @order = Order.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @order
  #         pdf = BackorderOrderStatusPdf.new(@order, @order_status)
  #         send_data pdf.render, filename: "Backordered Orders.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def cancelled_orders
  #   @order = Order.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @order
  #         pdf = CanceledOrderStatusPdf.new(@order, @order_status)
  #         send_data pdf.render, filename: "Cancelled Orders.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def completed_orders
  #   @order = Order.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @order
  #         pdf = CompletedOrderStatusPdf.new(@order, @order_status)
  #         send_data pdf.render, filename: "Completed Orders.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def invoiced_orders
  #   @order = Order.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @order
  #         pdf = InvoicedOrderStatusPdf.new(@order, @order_status)
  #         send_data pdf.render, filename: "Invoiced Orders.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def ordered_orders
  #   @order = Order.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @order
  #         pdf = OrderedOrderStatusPdf.new(@order, @order_status)
  #         send_data pdf.render, filename: "Ordered Orders.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def quoted_orders
  #   @order = Order.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @order
  #         pdf = QuotedOrderStatusPdf.new(@order, @order_status)
  #         send_data pdf.render, filename: "Quoted Orders.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def rejected_orders
  #   @order = Order.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @order
  #         pdf = RejectedOrderStatusPdf.new(@order, @order_status)
  #         send_data pdf.render, filename: "Rejected Orders.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end
  #
  # def shipped_orders
  #   @order = Order.all
  #   respond_to do |format|
  #     format.html
  #     format.pdf do
  #       if @order
  #         pdf = ShippedOrderStatusPdf.new(@order, @order_status)
  #         send_data pdf.render, filename: "Shipped Orders.pdf", type: "application/pdf", disposition: "inline"
  #       end
  #     end
  #   end
  # end


end

