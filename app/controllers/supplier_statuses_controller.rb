class SupplierStatusesController < ApplicationController
  before_action :set_supplier_status, only: [:show, :edit, :update, :destroy]

  # GET /supplier_statuses
  # GET /supplier_statuses.json
  def index
    @supplier_statuses = SupplierStatus.all
  end

  # GET /supplier_statuses/1
  # GET /supplier_statuses/1.json
  def show
    @supplier_statuses = SupplierStatus.all
    respond_to do |format|
      format.html
      format.pdf do
        if @supplier_status
          pdf = SupplierStatusesPdf.new(@supplier_status, @supplier)
          send_data pdf.render, filename: "Supplier Statuses Report.pdf", type: "application/pdf", disposition: "inline"
        end
      end
    end
  end

  # GET /supplier_statuses/new
  def new
    @supplier_status = SupplierStatus.new
  end

  # GET /supplier_statuses/1/edit
  def edit
  end

  # POST /supplier_statuses
  # POST /supplier_statuses.json
  def create
    @supplier_status = SupplierStatus.new(supplier_status_params)

    respond_to do |format|
      if @supplier_status.save
        format.html { redirect_to @supplier_status, notice: 'Supplier status was successfully created.' }
        format.json { render :show, status: :created, location: @supplier_status }
      else
        format.html { render :new }
        format.json { render json: @supplier_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /supplier_statuses/1
  # PATCH/PUT /supplier_statuses/1.json
  def update
    respond_to do |format|
      if @supplier_status.update(supplier_status_params)
        format.html { redirect_to @supplier_status, notice: 'Supplier status was successfully updated.' }
        format.json { render :show, status: :ok, location: @supplier_status }
      else
        format.html { render :edit }
        format.json { render json: @supplier_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /supplier_statuses/1
  # DELETE /supplier_statuses/1.json
  def destroy
    if @supplier_status.suppliers.count <= 0
    @supplier_status.destroy
    respond_to do |format|
      format.html { redirect_to supplier_statuses_url, notice: 'Supplier status was successfully destroyed.' }
      format.json { head :no_content }
    end
    else
      redirect_to supplier_statuses_path, notice: "Cannot delete supplier status because apply to existing suppliers"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_supplier_status
      @supplier_status = SupplierStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def supplier_status_params
      params.require(:supplier_status).permit(:description)
    end
end
