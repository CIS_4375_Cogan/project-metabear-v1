class Customer < ActiveRecord::Base
  belongs_to :customer_status
  has_many :customer_contacts
  has_many :employees, :through => :customer_contacts
  has_many :orders
  before_create { |customer| customer.customer_status_id = '1' };

  #Methods
  def full_Cust_name
    "#{fname} #{lname}"
  end
  def fname=(s)
    super s.titleize
  end
  def lname=(s)
    super s.titleize
  end
  def customer_name=(s)
    super s.titleize
  end
  def address=(s)
    super s.titleize
  end
  def city=(s)
    super s.titleize
  end

  def location
    "#{city}, #{state}"
  end



  validates :fname, :lname, :phone, :email, :address, :city,
            presence: true
  validates :phone,
            length: {minimum: 10, maximum: 11},
            numericality: true
  validates :fname, :lname, :state,
            format: {with: /\A[a-zA-Z]+\z/}
  validates :city,
            length: {maximum:  22}
  validates_format_of :email,
                      :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
  validates_format_of :city,
                      :with => /\A[^0-9`!@#\$%\^&*+_=]+\z/
  validates_format_of :address, :with => /[a-zA-Z\d ]/i,
                      :message => "can only contain letters and numbers."

end
