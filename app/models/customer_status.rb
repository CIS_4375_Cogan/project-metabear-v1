class CustomerStatus < ActiveRecord::Base
  has_many :customers

  def description=(s)
    super s.titleize
  end

  validates :description,
      presence: true,
      format: {with: /\A[a-zA-Z]+\z/}

end
