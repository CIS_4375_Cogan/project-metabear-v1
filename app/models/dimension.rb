class Dimension < ActiveRecord::Base
  has_many :pallets

  validates_format_of :name ,
                      :with => /[0-9]x[0-9]/, :on => :create,
                      :message => "Measurement is invalid"

end
