class Distributor < ActiveRecord::Base
  belongs_to :supplier
  belongs_to :pallet

  accepts_nested_attributes_for :pallet,
                                :reject_if => :all_blank

end
