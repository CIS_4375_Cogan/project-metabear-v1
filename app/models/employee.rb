class Employee < ActiveRecord::Base
  belongs_to :employee_status
  belongs_to :title
  has_many :customer_contacts
  has_many :customers, :through => :customer_contacts
  accepts_nested_attributes_for :customer_contacts,
                                :reject_if => :all_blank,
                                :allow_destroy => true
  accepts_nested_attributes_for :customers
  before_create { |employee| employee.employee_status_id = '1' };

  #Method
  def full_emp_name
    "#{fname} #{lname}"
  end
  def fname=(s)
    super s.titleize
  end
  def lname=(s)
    super s.titleize
  end
  def address=(s)
    super s.titleize
  end
  def city=(s)
    super s.titleize
  end

  #Validations
  validates :fname, :lname, :phone, :email, :address, :city,
            presence: true
  validates :phone,
            length: {minimum: 10, maximum: 11},
            numericality: true
  validates :fname, :lname, :city, :state,
            format: {with: /\A[a-zA-Z]+\z/}
  validates :city,
            length: {maximum:  22}
  validates_format_of :email,
                      :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
  validates_format_of :address, :with => /[a-zA-Z\d ]/i,
                      :message => "can only contain letters and numbers."

end
