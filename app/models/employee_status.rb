class EmployeeStatus < ActiveRecord::Base
  has_many :employee

  def emp_status
    "#{description}"
  end
  def description=(s)
    super s.titleize
  end

  validates :description,
            presence: true,
            format: {with: /\A[a-zA-Z]+\z/}

end
