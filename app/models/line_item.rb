class LineItem < ActiveRecord::Base
  belongs_to :pallet
  belongs_to :order
  before_save :calc_total
  before_save :this_date

  validates :quantity, presence: true
  validates_numericality_of :quantity,
                            :greater_than_or_equal_to =>  0

def calc_total
  self.line_item_total= self.pallet.price * self.quantity
end
  def this_date
    self.date = Time.now
  end


end
