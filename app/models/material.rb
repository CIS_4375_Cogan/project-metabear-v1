class Material < ActiveRecord::Base
  has_many :pallets

  def name=(s)
    super s.titleize
  end

  validates :name,
  presence: true,
  format: {with: /\A[a-zA-Z]+\z/}

end
