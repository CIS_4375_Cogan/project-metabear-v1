class Order < ActiveRecord::Base
  has_many :line_items
  belongs_to :order_status
  belongs_to :customer
  accepts_nested_attributes_for :line_items, reject_if: :all_blank, allow_destroy: true
  before_save :calculate_totals,
              :calculate_final,
              :this_date
              # :check_item,
  before_save :completed_status
  # validate :must_be_check

  def self.markup
    percentage_type = [0, 5, 10, 15]
  end

  private
    def calculate_totals
      self.quoted_amount = 0

      self.line_items.each do |i|
        unless i.marked_for_destruction?
          self.quoted_amount += (i.pallet.price * i.quantity)
        end
      end
    end
    # def must_be_check
    #   if self.shipped?
    #   errors.add(:base, "Approve need to be check") unless approved && shipped
    #   end
    #   end
    def check_item
      if self.approved?
        self.order_status_id = '3'
        else
        self.order_status_id = '1'
      end
    end
    def completed_status
      if self.approved?
        self.order_status_id = '8'
      end
    end
    def calculate_final
      self.final_amount= (quoted_amount.to_f * (markup.to_f/100)) + quoted_amount.to_f
    end

    def this_date
      self.date = Time.now
    end

end