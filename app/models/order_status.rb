class OrderStatus < ActiveRecord::Base
  has_many :orders

  def description=(s)
    super s.titleize
  end

  validates :description, presence: true
end
