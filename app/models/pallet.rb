class Pallet < ActiveRecord::Base
  belongs_to :pallet_status
  belongs_to :pallet_grade
  belongs_to :material
  belongs_to :dimension
  belongs_to :runner
  has_many :distributors
  has_many :suppliers, :through => :distributors, autosave: true
  has_many :line_items
  before_create { |pallet| pallet.pallet_status_id = '1' };


  #valdation
  validates :comment, presence: true
  validates :price, presence: true
   validates_numericality_of :price, :greater_than_or_equal_to => 0

  def full_Pallet_name
    "#{pallet_grade.level} #{material.name} #{dimension.name} #{comment}- $#{price}"
  end

  def pallet_name
    "#{pallet_grade.level} #{material.name} #{dimension.name} #{comment}"
  end


end
