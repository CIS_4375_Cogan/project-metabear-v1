class PalletGrade < ActiveRecord::Base
  has_many :pallets

  def description=(s)
    super s.titleize
  end


  validates :level, :description,
      presence: true
end
