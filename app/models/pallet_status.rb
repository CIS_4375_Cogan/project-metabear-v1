class PalletStatus < ActiveRecord::Base
  has_many :pallets

  def description=(s)
    super s.titleize
  end

  validates :description,
            presence: true

end
