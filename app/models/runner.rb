class Runner < ActiveRecord::Base
  has_many :pallets

  validates :number, :presence => true
  
end
