class Supplier < ActiveRecord::Base
  belongs_to :supplier_status
  has_many :distributors
  has_many :pallets, :through => :distributors, autosave: true
  accepts_nested_attributes_for :distributors,
                                :reject_if => :all_blank,
                                :allow_destroy => true

  accepts_nested_attributes_for :pallets, allow_destroy: true
  before_create { |supplier| supplier.supplier_status_id = '1' };


  #Methods
  def full_Sup_name
    "#{supplier_name} - #{contact_fn} #{contact_ln}"
  end
  def Sup_name
    "#{contact_fn} #{contact_ln}"
  end
  def contact_fn=(s)
    super s.titleize
  end
  def contact_ln=(s)
    super s.titleize
  end
  def company_name=(s)
    super s.titleize
  end
  def address=(s)
    super s.titleize
  end
  def city=(s)
    super s.titleize
  end

  def location
    "#{city}, #{state}"
  end

  #validation
  validates :phone, :supplier_name, :contact_fn, :contact_ln, :email, :address, :city,
            presence: true
  validates :phone,
            length: {minimum: 10, maximum: 11},
            numericality: true
  validates :contact_fn, :contact_ln, :city, :state,
            format: {with: /\A[a-zA-Z]+\z/}
  validates :city,
            length: {maximum:  22}
  validates_format_of :address, :with => /[a-zA-Z\d ]/i,
                      :message => "can only contain letters and numbers."

end