class Title < ActiveRecord::Base
  has_many :employees

  def position=(s)
    super s.titleize
  end


  validates :position,
    presence: true,
    format: {with: /\A[a-zA-Z]+\z/}

end
