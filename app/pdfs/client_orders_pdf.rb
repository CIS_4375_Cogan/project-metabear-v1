class ClientOrdersPdf < Prawn::Document
  include ActionView::Helpers::NumberHelper #calls helper functions for ease of formatting
  def initialize(customer, order)
    super(top_margin: 70)
    @customer = customer
    @order = order
    client_order_header
    line_order
  end

  def client_order_header
    text "Grembowiec & Associates", size: 25, style: :bold, :align => :center
    text "7211 Regency Square Blvd", size: 10, :align => :right
    text "Houston, Tx 77036", size: 10, :align => :right
    text "(713) 977-7776", size: 10, :align => :right
    customer = @customer.full_Cust_name.to_s
    text customer + "'s Orders as of (" + Time.now.in_time_zone('Central Time (US & Canada)').to_date.to_s + ")", size: 18, style: :bold
  end

  def line_order
    move_down 10
    table line_order_rows do
      row(0).font_style = :bold
      row(0).align = :center
      row(0).size = 14
      column(0).width = 75
      column(1..5).width = 93
      self.row_colors = ["DDDDDD", "E0FFFF"]
      self.header = true
    end

  end

  def line_order_rows
    [["Order Number", "Company", "Client", "Order Status", "Order Date",  "Total"]] +
        @customer.orders.map do |index|
          [index.id,@customer.company_name, @customer.full_Cust_name, index.order_status.description, index.date, number_to_currency(index.final_amount)]
        end
  end
end