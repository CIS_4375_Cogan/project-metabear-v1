class NewCustomersMonthlyPdf < Prawn::Document
  #This Report pulls all of the customer records that have been created within the last month and are "Active"
  include ActionView::Helpers::NumberHelper #calls helper functions for ease of formatting
  def initialize(customer_status, customer)
    super(top_margin: 70)
    @customer_status = customer_status
    @customer = customer
    customer_status_header
    line_status
  end

  def customer_status_header
    text "Grembowiec & Associates", size: 25, style: :bold, :align => :center
    text "7211 Regency Square Blvd", size: 10, :align => :right
    text "Houston, Tx 77036", size: 10, :align => :right
    text "(713) 977-7776", size: 10, :align => :right
    text "New Customers This Month (" + Time.now.in_time_zone('Central Time (US & Canada)').to_date.to_s + ")", size: 18, style: :bold
  end

  def line_status
    move_down 10
    table line_status_rows do
      row(0).font_style = :bold
      row(0).align = :center
      row(0).size = 14
      column(0..3).width = 135
      column(4).align = :center
      self.row_colors = ["DDDDDD", "E0FFFF"]
      self.header = true
    end

  end

  def line_status_rows
    [["Company Name", "Contact Name", "Phone Number", "Email Address"]] +
        Customer.where(:customer_status_id => CustomerStatus.find_by(description: 'Active'),created_at: [1.months.ago..Time.now]).map do |index|
          [index.company_name, index.full_Cust_name, number_to_phone(index.phone, area_code: true), index.email]
        end
  end

  #^ this pulls all the customers created within the last 3 days

end

#        Customer.includes(:customer_status).where('updated_at <= ? OR customer_status.description = inactive', 3.day.ago).references(:customer_status).map do |index|
