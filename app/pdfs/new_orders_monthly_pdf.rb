class NewOrdersMonthlyPdf < Prawn::Document
  #This Report pulls all of the order records that have been created within the last month and are "Active"
  include ActionView::Helpers::NumberHelper #calls helper functions for ease of formatting
  def initialize(order_status, order)
    super(top_margin: 70)
    @order_status = order_status
    @order = order
    order_status_header
    line_status
  end

  def order_status_header
    text "Grembowiec & Associates", size: 25, style: :bold, :align => :center
    text "7211 Regency Square Blvd", size: 10, :align => :right
    text "Houston, Tx 77036", size: 10, :align => :right
    text "(713) 977-7776", size: 10, :align => :right
    text "New Orders This Month (" + Time.now.in_time_zone('Central Time (US & Canada)').to_date.to_s + ")", size: 18, style: :bold
  end

  def line_status
    move_down 10
    table line_status_rows do
      row(0).font_style = :bold
      row(0).align = :center
      row(0).size = 14
      column(0..4).width = 108
      column(4).align = :center
      self.row_colors = ["DDDDDD", "E0FFFF"]
      self.header = true
    end

  end

  def line_status_rows
    [["Order Number", "Order Date", "Company", "Contact", "Total"]] +
        Order.where(created_at: [1.months.ago..Time.now]).map do |index|
          [index.id, index.date, index.customer.company_name, index.customer.full_Cust_name, number_to_currency( index.final_amount)]
        end
  end

  #^ this pulls all the orders created within the last 3 days

end

#        order.includes(:order_status).where('updated_at <= ? OR order_status.description = inactive', 3.day.ago).references(:order_status).map do |index|
