class OrderStatusesPdf < Prawn::Document
  include ActionView::Helpers::NumberHelper #calls helper functions for ease of formatting
  def initialize(order_status, order, customer)
    super(top_margin: 70) #:page_layout => :landscape
    @order_status = order_status
    @order = order
    @customer = customer
    order_status_header
    line_status
  end

  def order_status_header
    text "Grembowiec & Associates", size: 25, style: :bold, :align => :center
    text "7211 Regency Square Blvd", size: 10, :align => :right
    text "Houston, Tx 77036", size: 10, :align => :right
    text "(713) 977-7776", size: 10, :align => :right
    status = @order_status.description.to_s
    text status + " Orders as of (" + Time.now.in_time_zone('Central Time (US & Canada)').to_date.to_s + ")", size: 18, style: :bold
  end

  def line_status
    move_down 10
    table line_status_rows do
      row(0).font_style = :bold
      row(0).align = :center
      row(0).size = 14
      column(0).width = 75
      column(1..4).width = 116
      self.row_colors = ["DDDDDD", "E0FFFF"]
      self.header = true
    end

  end

  def line_status_rows
    [["Order Number", "Order Date", "Company", "Contact", "Total"]] +
        @order_status.orders.map do |index|
          [index.id, index.date, index.customer.company_name, index.customer.full_Cust_name, index.final_amount]
        end
  end
end