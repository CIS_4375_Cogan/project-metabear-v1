class PalletGradesPdf < Prawn::Document
  include ActionView::Helpers::NumberHelper #calls helper functions for ease of formatting
  def initialize(pallet_status, pallet, pallet_grade, runner, dimension)
    super(top_margin: 70)
    @pallet_status = pallet_status
    @pallet = pallet
    @pallet_grade = pallet_grade
    @runner = runner
    @dimension = dimension
    pallet_status_header
    line_status
  end

  def pallet_status_header
    text "Grembowiec & Associates", size: 25, style: :bold, :align => :center
    text "7211 Regency Square Blvd", size: 10, :align => :right
    text "Houston, Tx 77036", size: 10, :align => :right
    text "(713) 977-7776", size: 10, :align => :right
    grade_description = @pallet_grade.level
    text grade_description + " Grade Pallets as of (" + Time.now.in_time_zone('Central Time (US & Canada)').to_date.to_s + ")", size: 18, style: :bold
  end

  def line_status
    move_down 10
    table line_status_rows do
      row(0).font_style = :bold
      row(0).align = :center
      row(0).size = 14
      column(0..5).width = 108
      self.row_colors = ["DDDDDD", "E0FFFF"]
      self.header = true
    end

  end

  def line_status_rows
    [["Pallet Description", "Pallet Material", "Number of Runners", "Dimensions", "Status"]] +
        @pallet_grade.pallets.map do |index|
          [index.comment, index.material.name, index.runner.number, index.dimension.name, index.pallet_status.description]
        end
  end
end