class PalletRunnersPdf < Prawn::Document
  include ActionView::Helpers::NumberHelper #calls helper functions for ease of formatting
  def initialize(pallet_status, pallet, material, runner, dimension)
    super(top_margin: 70)
    @pallet_status = pallet_status
    @pallet = pallet
    @material = material
    @runner = runner
    @dimension = dimension
    pallet_runner_header
    line_status
  end

  def pallet_runner_header
    text "Grembowiec & Associates", size: 25, style: :bold, :align => :center
    text "7211 Regency Square Blvd", size: 10, :align => :right
    text "Houston, Tx 77036", size: 10, :align => :right
    text "(713) 977-7776", size: 10, :align => :right
    runners = @runner.number.to_s
    text "Pallets With " + runners + " Runners" + " as of (" + Time.now.in_time_zone('Central Time (US & Canada)').to_date.to_s + ")", size: 18, style: :bold
  end

  def line_status
    move_down 10
    table line_status_rows do
      row(0).font_style = :bold
      row(0).align = :center
      row(0).size = 14
      column(0..5).width = 108
      self.row_colors = ["DDDDDD", "E0FFFF"]
      self.header = true
    end

  end

  def line_status_rows
    [["Pallet Description", "Pallet Grade", "Pallet Material", "Dimensions", "Status"]] +
        @runner.pallets.map do |index|
          [index.comment, index.pallet_grade.level, index.material.name, index.dimension.name, index.pallet_status.description]
        end
  end
end