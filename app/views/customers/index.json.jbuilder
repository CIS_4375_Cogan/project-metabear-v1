json.array!(@customers) do |customer|
  json.extract! customer, :company_name, :id, :customer_status_id, :fname, :lname, :phone, :address, :city, :state, :email, :comment
  json.url customer_url(customer, format: :json)
end
