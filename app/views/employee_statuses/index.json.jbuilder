json.array!(@employee_statuses) do |employee_status|
  json.extract! employee_status, :id, :description
  json.url employee_status_url(employee_status, format: :json)
end
