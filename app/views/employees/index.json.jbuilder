json.array!(@employees) do |employee|
  json.extract! employee, :id, :employee_status_id, :title_id, :fname, :lname, :phone, :email, :address, :city, :state, :comment
  json.url employee_url(employee, format: :json)
end
