json.array!(@line_items) do |line_item|
  json.extract! line_item, :id, :order_id, :pallet_id, :quantity, :line_item_total, :date
  json.url line_item_url(line_item, format: :json)
end
