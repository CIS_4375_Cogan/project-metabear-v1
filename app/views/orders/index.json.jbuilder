json.array!(@orders) do |order|
  json.extract! order, :id, :customer_id, :order_status_id, :quoted_amount, :markup, :final_amount, :date, :approved, :description
  json.url order_url(order, format: :json)
end
