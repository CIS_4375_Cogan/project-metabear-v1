pdf.text @order.customer.full_Cust_name


orderitems = [["Pallet","Price","Dimension","Quantity"]]
orderitems += @order.line_items.map do |orderitem|
    [
        orderitem.pallet.full_Pallet_name,
        orderitem.pallet.price,
        orderitem.pallet.dimension.name,
        orderitem.quantity
    ]
end
pdf.table(orderitems, :header => true, :row_colors => ["CCEBF7","ffffff"],
:cell_style => { size: 7.5 }) do |table|
    table.row(0).font_style = :bold
    table.row(0).background_color = "D0D0D0"
end