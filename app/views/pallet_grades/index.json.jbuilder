json.array!(@pallet_grades) do |pallet_grade|
  json.extract! pallet_grade, :id, :level, :description
  json.url pallet_grade_url(pallet_grade, format: :json)
end
