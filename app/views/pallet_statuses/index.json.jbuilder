json.array!(@pallet_statuses) do |pallet_status|
  json.extract! pallet_status, :id, :description
  json.url pallet_status_url(pallet_status, format: :json)
end
