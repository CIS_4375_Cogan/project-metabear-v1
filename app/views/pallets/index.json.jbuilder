json.array!(@pallets) do |pallet|
  json.extract! pallet, :id, :pallet_grade_id, :pallet_status_id, :dimension_id, :runner_id, :material_id,:price, :comment
  json.url pallet_url(pallet, format: :json)
end
