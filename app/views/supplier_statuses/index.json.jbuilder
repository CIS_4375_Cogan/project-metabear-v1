json.array!(@supplier_statuses) do |supplier_status|
  json.extract! supplier_status, :id, :description
  json.url supplier_status_url(supplier_status, format: :json)
end
