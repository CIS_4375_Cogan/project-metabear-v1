json.array!(@suppliers) do |supplier|
  json.extract! supplier, :id, :supplier_status_id, :supplier_name, :contact_fn, :contact_ln, :phone, :email, :address, :city, :state, :comment
  json.url supplier_url(supplier, format: :json)
end
