Rails.application.routes.draw do
  resources :order_statuses
  resources :runners
  resources :orders
  resources :orders
  resources :dimensions
  resources :order_statuses
  resources :materials
  resources :line_items
  resources :pallets do
    member do
      get 'price'
  end
    end
  resources :customers
  resources :employees
  resources :customer_statuses
  resources :supplier_statuses
  resources :employee_statuses
  resources :pallet_statuses
  resources :suppliers
  resources :pallet_grades
  resources :titles

  #Additional Links
  get 'dashboard/index'
  get 'reports/new_orders_monthly'
  get 'reports/cancelled_orders_monthly'
  get 'reports/inactive_customers_monthly'
  get 'reports/inactive_suppliers_monthly'
  get 'reports/new_customers_monthly'
  get 'reports/new_suppliers_monthly'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'dashboard#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
