class CreatePalletGrades < ActiveRecord::Migration
  def change
    create_table :pallet_grades do |t|
      t.string :level
      t.text :description

      t.timestamps null: false
    end
  end
end
