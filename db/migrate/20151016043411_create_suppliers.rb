class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.integer :status_id
      t.string :supplier_name
      t.string :contact_fn
      t.string :contact_ln
      t.string :phone
      t.string :email
      t.string :address
      t.string :city
      t.string :state
      t.text :comment

      t.timestamps null: false
    end
  end
end
