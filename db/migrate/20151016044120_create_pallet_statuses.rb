class CreatePalletStatuses < ActiveRecord::Migration
  def change
    create_table :pallet_statuses do |t|
      t.string :description

      t.timestamps null: false
    end
  end
end
