class CreateSupplierStatuses < ActiveRecord::Migration
  def change
    create_table :supplier_statuses do |t|
      t.string :description

      t.timestamps null: false
    end
  end
end
