class CreateCustomerStatuses < ActiveRecord::Migration
  def change
    create_table :customer_statuses do |t|
      t.string :description

      t.timestamps null: false
    end
  end
end
