class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.integer :employee_status_id
      t.integer :title_id
      t.string :fname
      t.string :lname
      t.string :phone
      t.string :email
      t.string :address
      t.string :city
      t.string :state
      t.text :comment

      t.timestamps null: false
    end
  end
end
