class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.integer :customer_status_id
      t.string :fname
      t.string :lname
      t.string :phone
      t.string :address
      t.string :city
      t.string :state
      t.string :email
      t.text :comment

      t.timestamps null: false
    end
  end
end
