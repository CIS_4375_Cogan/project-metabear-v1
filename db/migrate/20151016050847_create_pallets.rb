class CreatePallets < ActiveRecord::Migration
  def change
    create_table :pallets do |t|
      t.integer :grade_id
      t.integer :supplier_id
      t.integer :pallet_status_id
      t.integer :line_item_id
      t.string :dimensions
      t.string :material
      t.text :comment

      t.timestamps null: false
    end
  end
end
