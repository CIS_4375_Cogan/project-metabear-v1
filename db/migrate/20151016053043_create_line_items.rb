class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.integer :quote_id
      t.integer :invoice_id
      t.integer :pallet_id
      t.integer :quantity
      t.decimal :line_item_total
      t.date :date

      t.timestamps null: false
    end
  end
end
