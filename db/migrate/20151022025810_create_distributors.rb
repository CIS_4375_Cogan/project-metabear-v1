class CreateDistributors < ActiveRecord::Migration
  def change
    create_table :distributors do |t|
      t.integer :pallet_id
      t.integer :supplier_id
      t.datetime :create_date

      t.timestamps null: false
    end
  end
end
