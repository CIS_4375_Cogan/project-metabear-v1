class CreateCustomerContacts < ActiveRecord::Migration
  def change
    create_table :customer_contacts do |t|
      t.integer :employee_id
      t.integer :customer_id
      t.datetime :create_date

      t.timestamps null: false
    end
  end
end
