class RenameColumnStatusinSupplierTable < ActiveRecord::Migration
  def change
    rename_column :suppliers, :status_id, :supplier_status_id
  end
end
