class RemoveColumnSupplierIdFromPallet < ActiveRecord::Migration
  def change
    remove_column :pallets, :supplier_id
  end
end
