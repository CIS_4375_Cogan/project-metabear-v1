class RenameColumnGradeIdInPallet < ActiveRecord::Migration
  def change
    rename_column :pallets, :grade_id, :pallet_grade_id
  end
end
