class AddPriceToPallet < ActiveRecord::Migration
  def change
    add_column :pallets, :price, :decimal, :precision => 10, :scale => 2
  end
end
