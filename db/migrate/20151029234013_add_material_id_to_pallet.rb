class AddMaterialIdToPallet < ActiveRecord::Migration
  def change
    add_column :pallets, :material_id, :integer
  end
end
