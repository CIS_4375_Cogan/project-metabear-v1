class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.integer :order_status_id
      t.decimal :quoted_amount
      t.decimal :markup
      t.decimal :final_amount
      t.date :date
      t.boolean :approved
      t.boolean :shipped
      t.string :description

      t.timestamps null: false
    end
  end
end
