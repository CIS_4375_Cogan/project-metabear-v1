class RemoveColumnDimensionAndMaterialInPallet < ActiveRecord::Migration
  def change
    remove_column :pallets, :dimensions
    remove_column :pallets, :material
  end
end
