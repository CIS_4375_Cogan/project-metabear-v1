class AddColumnDimensionIdMaterialIdAndRunnerIdInPallet < ActiveRecord::Migration
  def change
    add_column :pallets, :dimension_id, :integer
    add_column :pallets, :runner_id, :integer
  end
end
