class RemoveQuoteIdInvoiceIdToLineItem < ActiveRecord::Migration
  def change
    remove_column :line_items, :quote_id
    remove_column :line_items, :invoice_id
  end
end
