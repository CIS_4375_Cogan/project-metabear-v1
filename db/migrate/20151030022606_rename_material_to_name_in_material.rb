class RenameMaterialToNameInMaterial < ActiveRecord::Migration
  def change
    rename_column :materials, :description, :name
    rename_column :dimensions, :dimension, :name
  end
end
