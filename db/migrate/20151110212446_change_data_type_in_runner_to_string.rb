class ChangeDataTypeInRunnerToString < ActiveRecord::Migration
  def change
    change_column :runners, :number, :string
  end
end
