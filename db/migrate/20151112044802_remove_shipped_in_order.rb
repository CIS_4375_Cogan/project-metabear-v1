class RemoveShippedInOrder < ActiveRecord::Migration
  def change
    remove_column :orders, :shipped
  end
end
