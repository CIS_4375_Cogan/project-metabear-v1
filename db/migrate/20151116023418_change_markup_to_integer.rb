class ChangeMarkupToInteger < ActiveRecord::Migration
  def change
    change_column :orders, :markup, :integer
  end
end
