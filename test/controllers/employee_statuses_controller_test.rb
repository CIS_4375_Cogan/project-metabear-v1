require 'test_helper'

class EmployeeStatusesControllerTest < ActionController::TestCase
  setup do
    @employee_status = employee_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employee_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employee_status" do
    assert_difference('EmployeeStatus.count') do
      post :create, employee_status: { description: @employee_status.description }
    end

    assert_redirected_to employee_status_path(assigns(:employee_status))
  end

  test "should show employee_status" do
    get :show, id: @employee_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employee_status
    assert_response :success
  end

  test "should update employee_status" do
    patch :update, id: @employee_status, employee_status: { description: @employee_status.description }
    assert_redirected_to employee_status_path(assigns(:employee_status))
  end

  test "should destroy employee_status" do
    assert_difference('EmployeeStatus.count', -1) do
      delete :destroy, id: @employee_status
    end

    assert_redirected_to employee_statuses_path
  end
end
