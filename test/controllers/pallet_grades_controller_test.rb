require 'test_helper'

class PalletGradesControllerTest < ActionController::TestCase
  setup do
    @pallet_grade = pallet_grades(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pallet_grades)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pallet_grade" do
    assert_difference('PalletGrade.count') do
      post :create, pallet_grade: { description: @pallet_grade.description, level: @pallet_grade.level }
    end

    assert_redirected_to pallet_grade_path(assigns(:pallet_grade))
  end

  test "should show pallet_grade" do
    get :show, id: @pallet_grade
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pallet_grade
    assert_response :success
  end

  test "should update pallet_grade" do
    patch :update, id: @pallet_grade, pallet_grade: { description: @pallet_grade.description, level: @pallet_grade.level }
    assert_redirected_to pallet_grade_path(assigns(:pallet_grade))
  end

  test "should destroy pallet_grade" do
    assert_difference('PalletGrade.count', -1) do
      delete :destroy, id: @pallet_grade
    end

    assert_redirected_to pallet_grades_path
  end
end
