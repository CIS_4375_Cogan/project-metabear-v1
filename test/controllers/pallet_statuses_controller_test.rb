require 'test_helper'

class PalletStatusesControllerTest < ActionController::TestCase
  setup do
    @pallet_status = pallet_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pallet_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pallet_status" do
    assert_difference('PalletStatus.count') do
      post :create, pallet_status: { description: @pallet_status.description }
    end

    assert_redirected_to pallet_status_path(assigns(:pallet_status))
  end

  test "should show pallet_status" do
    get :show, id: @pallet_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pallet_status
    assert_response :success
  end

  test "should update pallet_status" do
    patch :update, id: @pallet_status, pallet_status: { description: @pallet_status.description }
    assert_redirected_to pallet_status_path(assigns(:pallet_status))
  end

  test "should destroy pallet_status" do
    assert_difference('PalletStatus.count', -1) do
      delete :destroy, id: @pallet_status
    end

    assert_redirected_to pallet_statuses_path
  end
end
