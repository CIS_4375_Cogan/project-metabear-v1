require 'test_helper'

class SupplierStatusesControllerTest < ActionController::TestCase
  setup do
    @supplier_status = supplier_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:supplier_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create supplier_status" do
    assert_difference('SupplierStatus.count') do
      post :create, supplier_status: { description: @supplier_status.description }
    end

    assert_redirected_to supplier_status_path(assigns(:supplier_status))
  end

  test "should show supplier_status" do
    get :show, id: @supplier_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @supplier_status
    assert_response :success
  end

  test "should update supplier_status" do
    patch :update, id: @supplier_status, supplier_status: { description: @supplier_status.description }
    assert_redirected_to supplier_status_path(assigns(:supplier_status))
  end

  test "should destroy supplier_status" do
    assert_difference('SupplierStatus.count', -1) do
      delete :destroy, id: @supplier_status
    end

    assert_redirected_to supplier_statuses_path
  end
end
